from django.db import models
from django.apps import apps
from django.conf import settings
# from accounts.models import User
# Create your models here.


class Categories(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=254)


class Passwords(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        Categories,
        on_delete=models.CASCADE
    )
    password = models.CharField(max_length=254, blank=True)
    platform = models.CharField(max_length=254)
    icon = models.CharField(max_length=254)


class Platform(models.Model):
    name = models.CharField(max_length=254, blank=True)
    icon = models.CharField(max_length=254, blank=True)
    background = models.CharField(max_length=254, blank=True)
    textColor = models.CharField(max_length=254, blank=True)
