from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect, render, reverse
from django.utils import timezone
from . forms import PassChoice, Titles, Mpass
from . models import Categories, Passwords
from . utils import Encrypter
import json
# Create your views here.


def PassView(request):
    queryset = Categories.objects.filter(user=request.user)

    if request.method == 'POST' and request.is_ajax:
        pwd = request.POST.get('data')
        pobjs = json.loads(pwd)
        print(request.session['masterp'])
        for x in pobjs:
            x['value'] = Encrypter().encrypt(
                x['value'], request.session['masterp'])
            Passwords.objects.filter(id=x['id']).update(password=x['value'])
        return JsonResponse({'success': True})
    return render(request, "encrypter/categories.html")


def cat_save(request):
    print(request.body)


def title_form(request):
    if request.method == 'POST' and request.is_ajax:
        form = Titles(request.POST)
        if form.is_valid():
            titles = form.save(commit=False)
            titles.user = request.user
            form.save()
        return JsonResponse({'success': True})

    else:
        formtitle = Titles()
        return HttpResponse(formtitle)


def StoreView(request):
    catid = request.GET.get('cat')
    return render(request, "encrypter/store.html", {"catid": catid})


def SetMaster(request):
    if request.method == 'POST' and request.is_ajax:
        form = Mpass(request.POST)
        mp = request.POST.get('master-key')
        pwds = Passwords.objects.filter(user=request.user)
        if len(pwds) != 0:
            for x in pwds:
                password = x.password.strip('\"')
                if password != "":
                    print(password)
                    pwdnew = bytes.decode(Encrypter().decrypt(
                        password[2:-1], mp))
                    if pwdnew == "":
                        return JsonResponse({'success': False})
            request.session['masterp'] = request.POST.get('master-key')
            return JsonResponse({'success': True})

    else:
        form = Mpass()
        return HttpResponse(Mpass)


def GetMaster(request):
    if request.method == 'POST' and request.is_ajax:
        if request.session.has_key('masterp'):
            if request.POST.get('demand') == 'check':
                return JsonResponse({'master': request.session.has_key('masterp')})
            elif request.POST.get('demand') == 'grab':
                return JsonResponse({'master': request.session['masterp']})

    return JsonResponse({'master': 'notset'})


def DelMaster(request):
    if request.method == 'POST' and request.is_ajax:
        if request.session.has_key('masterp'):
            del request.session['masterp']
            return JsonResponse({'master': 'deleted'})

    return JsonResponse({'master': 'notset'})
