from .models import Categories, Passwords, Platform
from rest_framework import serializers


class PasswordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Passwords
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Categories
        fields = '__all__'


class PlatformSerializer(serializers.ModelSerializer):
    class Meta:
        model = Platform
        fields = '__all__'
