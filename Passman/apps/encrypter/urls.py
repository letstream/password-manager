from . import views as v
from . import drfviews as dv
from rest_framework import routers
from django.contrib.auth.views import TemplateView
from django.urls import include, path

app_name = "encrypter"

router = routers.DefaultRouter()
router.register(r'passwords', dv.PasswordViewSet)
router.register(r'categories', dv.CategoryViewSet)
router.register(r'platforms', dv.PlatformViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path("", v.PassView, name="encrypter"),
    #path("locked", v.Locked, name="locked"),
    path("set", v.SetMaster, name="set"),
    path("get", v.GetMaster, name="get"),
    path("del", v.DelMaster, name="del"),
    path("store", v.StoreView, name="store"),
    path("cat_title", v.cat_save, name="categores"),
    path("title_form", v.title_form, name="title_form")
]
