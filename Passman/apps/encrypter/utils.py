# AES 256 encryption/decryption using pycrypto library

import base64
import hashlib
from Crypto.Cipher import AES
from Crypto import Random


class Encrypter:
    BLOCK_SIZE = 16
    def pad(self, s): return s + (self.BLOCK_SIZE - len(s) %
                                  self.BLOCK_SIZE) * chr(self.BLOCK_SIZE - len(s) % self.BLOCK_SIZE)

    def unpad(self, s): return s[:-ord(s[len(s) - 1:])]

    def encrypt(self, raw, password):
        private_key = hashlib.sha256(password.encode("utf-8")).digest()
        raw = self.pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(private_key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))

    def decrypt(self, enc, password):
        private_key = hashlib.sha256(password.encode("utf-8")).digest()
        enc = base64.b64decode(enc)
        iv = enc[:16]
        cipher = AES.new(private_key, AES.MODE_CBC, iv)
        return self.unpad(cipher.decrypt(enc[16:]))


# First let us encrypt secret message
#encrypted = encrypt("This is a secret message", password)
# print(encrypted)

# Let us decrypt using our original password
#decrypted = decrypt(encrypted, password)
# print(bytes.decode(decrypted))
