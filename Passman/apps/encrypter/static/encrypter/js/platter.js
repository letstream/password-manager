$(document).ready(function() {
  var user = getCookie("user");
  var cat = getCookie("cat");
  let rtcards = [];
  let cards;
  var platforms = [];
  function fetchcards() {
    $.ajax({
      url: "http://127.0.0.1:8000/encrypter/api/platforms/",
      crossOrigin: true,
      dataType: "json",
      type: "GET",
      async: "false",
      success: function(data) {
        cards = data;
        $.ajax({
          url: "http://127.0.0.1:8000/encrypter/api/passwords/?user=" + user,
          crossOrigin: true,
          dataType: "json",
          type: "GET",
          async: "false",
          success: function(removecats) {
            removecats = removecats.results;
            for (let j = 0; j < removecats.length; j++) {
              platforms.push(removecats[j].platform);
            }
            showplats(cards.results, platforms);
          }
        });
      }
    });
  }

  function showplats(cards, platforms) {
    $(".platrow").html("");
    $.each(cards, function(i, v) {
      var searchField = $("#search").val();
      var expression = new RegExp(searchField, "i");
      if (v.name.search(expression) != -1 || searchField == "") {
        if (platforms.indexOf(v.name) == -1) {
          $(".platrow").append(
            '<div class="col-3 col-md-2 col-lg-2" >' +
              '<div id="' +
              $(v)[0].id +
              '" class="card card-store" style="background:' +
              $(v)[0].background +
              ";color:" +
              $(v)[0].textColor +
              '">' +
              '<div class="card-body">' +
              '<span class="card-title">' +
              $(v)[0].icon +
              "</span>" +
              '<p class="card-text">' +
              $(v)[0].name +
              "</p>" +
              "</div>" +
              "</div>" +
              "</div>"
          );
        }
      }
    });
    rotcards(rtcards);
  }

  $("#search").keyup(function() {
    showplats(cards.results, platforms);
  });

  //
  //

  function rotcards(rtcards) {
    let i;
    for (i = 0; i < rtcards.length; i++) {
      let selector = "#" + rtcards[i];
      $(selector).css("transform", "rotateZ(10deg)");
    }
    checkrot();
  }

  function checkrot() {
    if (rtcards.length == 0) {
      $(".fa-arrow-circle-right").css("visibility", "hidden");
    } else {
      $(".fa-arrow-circle-right").css("visibility", "visible");
    }
  }

  $(document).on(
    "click",
    ".card-store",
    debounce(function(event) {
      let rotval = $(this).css("transform");
      if (rotval == "matrix(0.994522, 0.104528, -0.104528, 0.994522, 0, 0)") {
        $(this).css("transform", "rotateZ(0deg)");
        rtcards.pop($(this).attr("id"));
      } else {
        $(this).css("transform", "rotateZ(6deg)");
        rtcards.push($(this).attr("id"));
      }
      checkrot();
    }, 350)
  );

  //Debounce Function
  fetchcards();

  $(document).on(
    "click",
    ".fa-arrow-circle-right",
    debounce(function(event) {
      $(".platrow").css("opacity", "0.4");
      $(".lds-facebook").css("display", "inline-block");
      addpass = [];

      var csrftoken = getCookie("csrftoken");
      //addpass.push("csrfmiddlewaretoken:" + csrftoken);

      $.each(rtcards, function(i, v) {
        var obj = {
          password: "",
          platform: cards.results[v - 1].name,
          icon: cards.results[v - 1].icon,
          user: user,
          category: cat
        };
        addpass.push(obj);
      });
      console.log(JSON.stringify(addpass));
      $.ajax({
        url: "http://127.0.0.1:8000/encrypter/api/passwords/",
        type: "POST",
        dataType: "json",
        data: JSON.stringify(addpass),
        beforeSend: function(xhr) {
          xhr.setRequestHeader("X-CSRFToken", csrftoken);
          xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function() {
          $(".platrow").css("opacity", "1");
          $(".lds-facebook").css("display", "none");
          window.location.href = "http://127.0.0.1:8000/encrypter/";
        }
      });
    }, 200)
  );
});
