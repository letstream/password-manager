from . models import Passwords, Categories, Platform
from rest_framework import viewsets, status
from rest_framework.response import Response
from .utils import Encrypter
from . serializers import PasswordSerializer, CategorySerializer, PlatformSerializer


class PasswordViewSet(viewsets.ModelViewSet):
    queryset = Passwords.objects.all()
    serializer_class = PasswordSerializer

    def create(self, request, pk=None, company_pk=None, project_pk=None):
        is_many = True if isinstance(request.data, list) else False

        serializer = self.get_serializer(data=request.data, many=is_many)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_queryset(self):
        queryset = self.queryset
        if self.request.query_params.get('cat'):
            cat = self.request.query_params.get('cat')
            query_set = queryset.filter(category=cat)
            print(self.request.session['masterp'])
            for x in query_set:
                # print()
                # print(len(x.password[2:-1]))
                # print(type(x.password))
                if x.password != "":
                    x.password = bytes.decode(Encrypter().decrypt(x.password[2:-1],
                                                                  self.request.session['masterp']))

        elif self.request.query_params.get('user'):
            user = self.request.query_params.get('user')
            query_set = queryset.filter(user=user)

        else:
            query_set = queryset.all()
        return query_set


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Categories.objects.all()
    serializer_class = CategorySerializer


class PlatformViewSet(viewsets.ModelViewSet):
    queryset = Platform.objects.all()
    serializer_class = PlatformSerializer
