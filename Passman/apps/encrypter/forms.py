from . models import Passwords, Categories
from django import forms
from django.forms import ModelForm, Form
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Row, Column, Field
from crispy_forms.bootstrap import PrependedText


class PassChoice(ModelForm):
    p = []
    mpassword = forms.CharField(max_length=254, widget=forms.PasswordInput())

    class Meta:
        model = Passwords
        fields = '__all__'
        widgets = {
            'password1': forms.PasswordInput(),
            'password2': forms.PasswordInput(),
            'password3': forms.PasswordInput(),
            'password4': forms.PasswordInput(),
            'password5': forms.PasswordInput(),
            'password6': forms.PasswordInput(),
            'password7': forms.PasswordInput(),
            'password8': forms.PasswordInput(),
            'password9': forms.PasswordInput(),
            'password10': forms.PasswordInput(),
        }

    def __init__(self, *args, **kwargs):
        super(PassChoice, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Fieldset(
                Row(
                    Column(PrependedText('password1',
                                         '@', css_class='pwd-field')),
                    Column(PrependedText('password2',
                                         '@', css_class='pwd-field')),
                    Column(PrependedText('password3',
                                         '@', css_class='pwd-field')),
                    Column(PrependedText('password4',
                                         '@', css_class='pwd-field')),
                    Column(PrependedText('password5',
                                         '@', css_class='pwd-field')),
                    Column(PrependedText('password6',
                                         '@', css_class='pwd-field')),
                    Column(PrependedText('password7',
                                         '@', css_class='pwd-field')),
                    Column(PrependedText('password8',
                                         '@', css_class='pwd-field')),
                    Column(PrependedText('password9',
                                         '@', css_class='pwd-field')),
                    Column(PrependedText('password10',
                                         '@', css_class='pwd-field')),
                    Column(PrependedText('password11',
                                         '@', css_class='pwd-field'))
                )),

            # ButtonHolder(
            # Submit('submit', 'Submit', css_class='button white')
        )

    def clean(self):

        super(PassChoice, self).clean()
        for field in self.fields:
            self.p.append(self.cleaned_data.get(field))


class Titles(ModelForm):

    class Meta:
        model = Categories
        fields = '__all__'
        exclude = ['user']

    def __init__(self, *args, **kwargs):
        super(Titles, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Fieldset(
                'Title',
                Row(
                    Column('title', css_class='pwd-field'))),)

    def clean(self):
        super(Titles, self).clean()


class Mpass(Form):
    mpassword = forms.CharField(max_length=254, widget=forms.PasswordInput())

    def clean(self):
        super(Mpass, self).clean()
